﻿using System;
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Services;
using Otus.Teaching.Linq.ATM.DataAccess;

namespace Otus.Teaching.Linq.ATM.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("Старт приложения-банкомата...");

            var atmManager = CreateATMManager();

            //TODO: Далее выводим результаты разработанных LINQ запросов
            System.Console.WriteLine("Введите логин");
            string login = System.Console.ReadLine();
            System.Console.WriteLine("Введите пароль");
            string password = System.Console.ReadLine();

            try
            {
                //Task01
                System.Console.WriteLine("\nИнформация о пользователе:");
                System.Console.WriteLine(atmManager.AccountInfo(login, password));

                //Task02
                System.Console.WriteLine("\nИнформация о всех счетах пользователя:");
                var accounts = atmManager.AllAccounts(login, password);
                foreach(var item in accounts)
                    System.Console.WriteLine(item);

                //Task03
                atmManager.AllAccountsWithHistory(login, password);
            }
            catch (Exception exc)
            {
                System.Console.WriteLine(exc.Message);
            }

            //Task04
            atmManager.InputCashOpertions();

            //Task05
            atmManager.AboveN(6000);

            System.Console.WriteLine("Завершение работы приложения-банкомата...");
        }

        static ATMManager CreateATMManager()
        {
            using var dataContext = new ATMDataContext();
            var users = dataContext.Users.ToList();
            var accounts = dataContext.Accounts.ToList();
            var history = dataContext.History.ToList();
                
            return new ATMManager(accounts, users, history);
        }
    }
}
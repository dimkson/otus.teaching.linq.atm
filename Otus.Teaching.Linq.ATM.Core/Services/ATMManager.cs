﻿using System.Collections.Generic;
using Otus.Teaching.Linq.ATM.Core.Entities;
using System.Linq;

namespace Otus.Teaching.Linq.ATM.Core.Services
{
    public class ATMManager
    {
        public IEnumerable<Account> Accounts { get; private set; }
        
        public IEnumerable<User> Users { get; private set; }
        
        public IEnumerable<OperationsHistory> History { get; private set; }
        
        public ATMManager(IEnumerable<Account> accounts, IEnumerable<User> users, IEnumerable<OperationsHistory> history)
        {
            Accounts = accounts;
            Users = users;
            History = history;
        }

        //TODO: Добавить методы получения данных для банкомата

        #region Methods
        //Вывод информации о заданном аккаунте по логину и паролю.
        public User AccountInfo(string login, string password)
        {
            var accInfo = Users.Where(user => user.Login == login && user.Password == password).FirstOrDefault();
            return accInfo ?? throw new System.ResourceNotFoundException("Не существует пользователя с таким логином/паролем.");
        }

        //Вывод данных о всех счетах заданного пользователя.
        public IEnumerable<Account> AllAccounts(string login, string password)
        {
            return Accounts.Where(account => account.UserId == AccountInfo(login, password).Id);
        }

        //Вывод данных о всех счетах заданного пользователя, включая историю по каждому счету.
        public void AllAccountsWithHistory(string login, string password)
        {
            var accounts = AllAccounts(login, password).GroupJoin(
                                                    History,
                                                    account => account.Id,
                                                    strory => strory.AccountId,
                                                    (account, strory) => new
                                                    {
                                                        Id = account.Id,
                                                        OpeningDate = account.OpeningDate,
                                                        CashAll = account.CashAll,
                                                        Histories = strory
                                                    });

            System.Console.WriteLine($"\nВсе счета пользователя {login}, включая историю по каждому счету");
            foreach (var item in accounts)
            {
                System.Console.WriteLine($"Id:{item.Id}, OpeningDate: {item.OpeningDate}, " +
                    $"CashAll: {item.CashAll}");
                foreach (var history in item.Histories)
                    System.Console.WriteLine(history);
            }
        }

        //Вывод данных о всех операциях пополнения счета с указанием владельца каждого счета.
        public void InputCashOpertions()
        {
            var inCash = History.Where(
                                    h => h.OperationType == OperationType.InputCash)
                                    .Join(
                                        Accounts,
                                        f => f.AccountId,
                                        s => s.Id,
                                        (f, s) => new
                                        {
                                            Name = Users.Where(us => us.Id == s.UserId).Select(e => e.SurName).First(),
                                            Id = f.Id,
                                            OperationDate = f.OperationDate,
                                            CashSum = f.CashSum
                                        });

            System.Console.WriteLine("\nВсе операции пополнения счета");
            foreach (var item in inCash)
                System.Console.WriteLine(item);

        }

        //Вывод данных о всех пользователях у которых на счёте сумма больше N.
        public void AboveN(decimal n)
        {
            var above = Accounts.Where(c => c.CashAll > n).
                                                    Join(
                                                       Users,
                                                       f => f.UserId,
                                                       s => s.Id,
                                                       (f, s) => new
                                                       {
                                                           s.FirstName,
                                                           s.SurName,
                                                           f.CashAll
                                                       });

            System.Console.WriteLine($"\nВсе пользователи у которых на счете больше {n}");
            foreach (var item in above)
                System.Console.WriteLine(item);
        } 
        #endregion
    }
}